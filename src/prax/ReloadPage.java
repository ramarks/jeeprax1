package prax;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ReloadPage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		Integer reloadCount = (Integer) session.getAttribute("count");
		
		if (reloadCount == null) {
			reloadCount = 0;
		}
		
		session.setAttribute("count", ++reloadCount);
		
		response.getWriter().println("Reload count: " + session.getAttribute("count"));
	}

}
