package prax;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class WelcomePage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		
		String form = "<form method='POST'>" +
						"Sisesta oma nimi   <input name='name' />   " +
						"<input type='submit' value='Sisesta' />" +
						"</form>";
		response.getWriter().println(form);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		
		response.getWriter().println("Sinu nimi on " + request.getParameter("name"));
	}

}
