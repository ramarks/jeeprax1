package prax;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TimePage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("image/png");
		ServletOutputStream outputStream = response.getOutputStream();
		
		try {
		      int width = 200, height = 200;

		      // TYPE_INT_ARGB specifies the image format: 8-bit RGBA packed
		      // into integer pixels
		      BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

		      Graphics2D ig2 = bi.createGraphics();

		      Font font = new Font("TimesRoman", Font.BOLD, 30);
		      ig2.setFont(font);
//		      String message = "www.java2s.com!";
		      DateFormat dateFormat = new SimpleDateFormat("hh:mm:ss");
		      Date date = new Date();
		      String message = dateFormat.format(date);
		      FontMetrics fontMetrics = ig2.getFontMetrics();
		      int stringWidth = fontMetrics.stringWidth(message);
		      int stringHeight = fontMetrics.getAscent();
		      ig2.setPaint(Color.black);
		      ig2.drawString(message, (width - stringWidth) / 2, height / 2 + stringHeight / 4);

		      ImageIO.write(bi, "PNG", outputStream);

		    } catch (IOException ie) {
		      ie.printStackTrace();
		    }

	}

}
